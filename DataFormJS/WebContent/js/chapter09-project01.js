/* add code here  */
window.addEventListener("load",function() {
	
var highlightClass = document.getElementsByClassName("hilightable");
	
	for(i=0;i<highlightClass.length;i++){
		highlightClass[i].addEventListener("focus",focus);
		highlightClass[i].addEventListener("blur",blur);
	}

	function focus(e) {
		e.target.classList.toggle("highlight");
	}
	
	function blur(e) {
		e.target.classList.toggle("highlight");
	}
	
	
var submitForm =document.getElementById("mainForm");
	submitForm.addEventListener("submit",submit);
	
	function submit(e){
		var requiredElement = document.getElementsByClassName("required");
		var emptyField = false; 
		
		for(i=0;i<requiredElement.length;i++){
			if(requiredElement[i].value == ""){
				requiredElement[i].classList.add("error");
				emptyField=true;
			}else{
				requiredElement[i].classList.remove("error");
			}
		}
	
		if(emptyField==true){
			e.preventDefault();
		}	
	}
});